// Handle prefixed versions
navigator.getUserMedia = (navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia);

// WebAudio API
var context;

// State
var me = {};
var myStream;
var peers = {};

var mixedOutput;
var rec1, rec2;
var recording = false;
var isFirefox = !!navigator.mozGetUserMedia;
var chunks, time, prefix;

init();

// Start everything up
function init() {
  if (!navigator.getUserMedia) return unsupported();

  // Tries to use WebAudio API
  try {
    // Fix up for prefixing
    window.AudioContext = window.AudioContext||window.webkitAudioContext;
    context = new AudioContext();
    mixedOutput = context.createMediaStreamDestination();
  } catch(e) {
    display('Web Audio API is not supported in this browser');
  }

  getLocalAudioStream(function(err, stream) {
    if (err || !stream) return;

    connectToPeerJS(function(err) {
      if (err) return;

      registerIdWithServer(me.id);
      if (call.peers.length) callPeers();
      else displayShareMessage();
    });
  });
}

// Connect to PeerJS and get an ID
function connectToPeerJS(cb) {
  display('Connecting to PeerJS...');
  try {

    me = new Peer({key: API_KEY});

    me.on('call', handleIncomingCall);
    
    me.on('open', function() {
      display('Connected.');
      display('ID: ' + me.id);
      cb && cb(null, me);
    });
    
    me.on('error', function(err) {
      display(err);
      cb && cb(err);
    });
  } catch (err) { display(err.message); }
}

// Add our ID to the list of PeerJS IDs for this call
function registerIdWithServer() {
  display('Registering ID with server...');
  $.post('/' + call.id + '/addpeer/' + me.id);
} 

// Remove our ID from the call's list of IDs
function unregisterIdWithServer() {
  $.post('/' + call.id + '/removepeer/' + me.id);
}

// Call each of the peer IDs using PeerJS
function callPeers() {
  call.peers.forEach(callPeer);
}

function callPeer(peerId) {
  display('Calling ' + peerId + '...');
  var peer = getPeer(peerId);
  peer.outgoing = me.call(peerId, myStream);
  
  peer.outgoing.on('error', function(err) {
    display(err);
  });

  peer.outgoing.on('stream', function(stream) {
    display('Connected to ' + peerId + '.');
    addIncomingStream(peer, stream);
  });
}

// When someone initiates a call via PeerJS
function handleIncomingCall(incoming) {
  display('Answering incoming call from ' + incoming.peer);
  var peer = getPeer(incoming.peer);
  peer.incoming = incoming;
  incoming.answer(myStream);
  peer.incoming.on('stream', function(stream) {
    addIncomingStream(peer, stream);
  });
}

// Add the new audio stream. Either from an incoming call, or
// from the response to one of our outgoing calls
function addIncomingStream(peer, stream) {
  display('Adding incoming stream from ' + peer.id);
  peer.incomingStream = stream;
  
  var microphone = context.createMediaStreamSource(stream);
  microphone.connect(mixedOutput);

  playStream(stream);
}

// Create an <audio> element to play the audio stream
function playStream(stream) {
  var audio = $('<audio autoplay />').appendTo('body');
  audio[0].src = (URL || webkitURL || mozURL).createObjectURL(stream);
}

function getLocalAudioStream(cb) {
  display('Trying to access your microphone. Please click "Allow".');

  navigator.getUserMedia (
    {video: false, audio: true},

    function success(audioStream) {
      display('Microphone is open.');
      myStream = audioStream;

      var microphone = context.createMediaStreamSource(audioStream);
      microphone.connect(mixedOutput);

      if (cb) cb(null, myStream);
    },

    function error(err) {
      display('Couldn\'t connect to microphone. Reload the page to try again.');
      if (cb) cb(err);
    }
  );
}

// ==============================================================================
var finishUpload = function(data) {
  var msg = (data.upload) ? 'Audio file uploaded with success.' : 'Failed to upload audio file.';
  console.log(msg);
};

var uploadStream = function(rec) {
  //rec.save('conv_' + chunks.toString());
  //chunks++;
  
  rec.getDataURL(function(audioDataURL) {

    var params = {};
    params.data = audioDataURL;
    params.type = 'audio/wav';
    params.name = prefix + '_' + chunks.toString();

    $.ajax({
      url        : '/upload',
      dataType   : 'json',
      contentType: 'application/json; charset=UTF-8', // This is the money shot
      data       : JSON.stringify(params),
      type       : 'POST',
      success    : finishUpload
    });

    chunks++;

  });
};

var getChunk = function() {
  if (recording) {

    if (chunks % 2 == 0) {
      console.log('Stopped Rec1, started Rec2.');

      rec2.startRecording();
      rec1.stopRecording(function() {
        uploadStream(rec1);
      });
    } else {
      console.log('Stopped Rec2, started Rec1.');

      rec1.startRecording();
      rec2.stopRecording(function() {
        uploadStream(rec2);
      });
    }
  }
};

$('#rec').click(function() {

  if (!recording) {
    chunks = 0;
    time = 120; // Every 2 min is a chunk of audio
    prefix = Date.now();

    // Create recorders
    recParams = { recorderType: StereoAudioRecorder,
                  disableLogs: false,
                  numberOfAudioChannels: 1, // or leftChannel:true
                  //bufferSize: 16384,
                  //sampleRate: 96000
                };

    rec1 = RecordRTC(mixedOutput.stream, recParams);
    rec2 = RecordRTC(mixedOutput.stream, recParams);

    // Using rec1
    rec1.startRecording();
    console.log('Started Rec1.');

    window.setInterval(function(){
      getChunk();
    }, time * 1000);

  }

  else {
    // Define: if chunks is odd, use recorder, if not, recorderAux
    var rec = (chunks % 2 == 0) ? rec1 : rec2;

    if (rec === rec1) console.log('Stopped Rec1.');
    else console.log('Stopped Rec2.');

    rec.stopRecording(function() {
      uploadStream(rec);
    });

  }

  recording = !recording;

  buttonText = recording ? 'Stop' : 'Start';
  $('#rec').text(buttonText + ' Recording');

});

////////////////////////////////////
// Helper functions
function getPeer(peerId) {
  return peers[peerId] || (peers[peerId] = {id: peerId});
}

function displayShareMessage() {
  display('Give someone this URL to chat.');
  display('<input type="text" value="' + location.href + '" readonly>');
  
  $('#display input').click(function() {
    this.select();
  });
}

function unsupported() {
  display("Your browser doesn't support getUserMedia.");
}

function display(message) {
  $('<div />').html(message).appendTo('#display');
}