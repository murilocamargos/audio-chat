PeerJS / RecordRTC / Amazon S3 Audio Chat
=================

This is a barebones proof-of-concept WebRTC audio chat app built using [PeerJS](http://peerjs.com). It uses a simple Node backend that keeps track of peer IDs for each call. Also includes the features of recording the conversations with [RecordRTC](http://recordrtc.org) and uploading them in an [Amazon S3](https://aws.amazon.com/s3) bucket.

-------------------------------
## How it works
This app uses the new [WebRTC APIs](http://www.html5rocks.com/en/tutorials/webrtc/basics/) to connect directly to other users' browsers. Here's how it all works.

#### 1. Access your Microphone with getUserMedia()
When you land on the call page, your browser will prompt you to allow the page to access your microphone. This is done by calling [`navigator.getUserMedia()`](https://developer.mozilla.org/en-US/docs/NavigatorUserMedia.getUserMedia).

After allowing microphone access, you have access to a [`LocalMediaStream`](https://developer.mozilla.org/en-US/docs/Web/API/MediaStream_API#LocalMediaStream). There lots of other things you can do with this stream using the [Web Audio API](http://www.html5rocks.com/en/tutorials/webaudio/intro/), here it's used to mixed all stream into one for recording.

Here's what we're doing:
```javascript
// handle browser prefixes
navigator.getUserMedia = (navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia);
  
// Get access to microphone
navigator.getUserMedia (
  // Only request audio
  {video: false, audio: true},
    
  // Success callback
  function success(localAudioStream) {
    // Do something with audio stream
  },
  // Failure callback
  function error(err) {
    // handle error
  }
);
```

#### 2. Using the Web Audio API to mix the streams
We're using the API to mix all streams in the call so we can record it. First, we need to create an `AudioContext` and a variable that will be used to connect each stream (`mixedOutput`).
```javascript
// Tries to use WebAudio API
// Fix up for prefixing
window.AudioContext = window.AudioContext||window.webkitAudioContext;
context = new AudioContext();
mixedOutput = context.createMediaStreamDestination();
```

Then, everytime a new user arrives at the chatroom, we just add the incoming stream to `mixedOutput`:
```javascript
var microphone = context.createMediaStreamSource(stream);
microphone.connect(mixedOutput);
```

#### 3. Connect to PeerJS
[PeerJS](http://peerjs.com) takes care of the hairier parts of using WebRTC for us (STUN, TURN, signaling). To connect, you need to include the PeerJS javascript, then make a new instance of [`Peer`](http://peerjs.com/docs/#api):

```javascript
var me = new Peer({key: API_KEY});
me.on('open', function() {
  console.log('My PeerJS ID is:', me.id);
});
```

The `open` event will fire once you're connected.

For this demo, we're sending this ID to the server, where it's associated with this call ID. When someone else opens the call we then pass them everyone else's PeerJS IDs.

#### 4. Call peers
When other people open the call page, they'll have your PeerJS ID (and maybe other people's). These IDs are then used to connect to each other.

Each `Peer` instance has a `.call()` method that takes a peer's ID, your `LocalMediaStream` as arguments and returns a PeerJS `MediaConnection` object.

```javascript
var outgoing = me.call(peerId, myAudioStream);
```

This `MediaConnection` object will fire a `stream` event when the other person answers your call with their own audio stream.

```javascript
outgoing.on('stream', function(stream) {
  // Do something with this audio stream
});
```

When receiving a `.call()`, your `Peer` instance will fire a `call` event, which gets passes an instance of a PeerJS `MediaConnection`. You then listen for the `stream` event on this object to get incoming audio stream:

```javascript
me.on('call', function(incoming) {
  incoming.on('stream', function(stream) {
    // Do something with this audio stream
  });
});
```

Once all this happens, both parties should have an audio stream from the other person.

#### 5. Play audio
There are two ways to play an audio stream: the Web Audio API, and the HTML5 `<audio>` element. Firefox supports both, but Chrome currently [doesn't support](https://code.google.com/p/chromium/issues/detail?can=2&q=121673&colspec=ID%20Pri%20M%20Iteration%20ReleaseBlock%20Cr%20Status%20Owner%20Summary%20OS%20Modified&id=121673) playing a WebRTC stream using the Web Audio API, so we're using the `<audio>` element here:

```javascript
function playStream(stream) {
  var audio = $('<audio autoplay />').appendTo('body');
  audio[0].src = (URL || webkitURL || mozURL).createObjectURL(stream);
}
```

We use the new `URL.createObjectURL()` method to get a URL that the `<audio>` element can stream.

Also note the `autoplay` attribute. You don't have to have this, but if you don't use it you'll need to manually call the `.play()` on the `<audio>` element.

*That's it. You can read more about using PeerJS in their [documentation](http://peerjs.com/docs/).*

#### 6. Recording data with RecordRTC
The user that start a call has control over the recorder. He can start/stop the recorder anytime he wants. We use the [RecordRTC](http://recordrtc.org/RecordRTC.html) for Node to set up two recorders that works togheter to record continously.

```javascript
// Rec parameters, you can check RecordRTC's doc to set this up to meet your needs.
recParams = { recorderType: StereoAudioRecorder };
// Two recorders are created with the mixed stream as input
rec1 = RecordRTC(mixedOutput.stream, recParams);
rec2 = RecordRTC(mixedOutput.stream, recParams);
```

Two recorders are created so one can work while the other rests, we used this approach because the content in each recorder will be uploaded to an Amazon S3 bucket and this takes time. We cannot stop recording while `rec1` stream is being uploaded, so we start `rec2` and just alternate between them.

The audio data is uploaded in chunks, it is currently set to send an audio file every two minutes but this could be changed to meet your needs.

#### 7. Uploading audio chunks into an Amazon S3 bucket
Every two minutes the upload function is called in the client-side, it just sends a `POST` request to the server with the file parameters such as name, type and the actual data.
```javascript
var uploadStream = function(rec) {
    rec.getDataURL(function(audioDataURL) {
        var params = {};
        params.data = audioDataURL;
        params.type = 'audio/wav';
        params.name = prefix + '_' + chunks.toString();
    
        $.ajax({
            url        : '/upload',
            dataType   : 'json',
            contentType: 'application/json; charset=UTF-8',
            data       : JSON.stringify(params),
            type       : 'POST',
            success    : finishUpload // callback
        });
        
        chunks++;
    });
};
```
***Note**: If you want to save this files directly in your hard drive instead, you can use the method `save` of the `RecordRTC` object*

At the server-side, we're using the [`Knox`](https://github.com/Automattic/knox) API to upload the files, which is a Node [Amazon S3](https://aws.amazon.com/s3/?) client. What it does is set a connection with you bucket using the access keys you provide.

```javascript
router.post('/upload', function(req, res) {
    // Knox client to access Amazon S3 bucket
    var client = knox.createClient({
      key: process.env.AWS_ACCESS_KEY_ID || config.AWS_ACCESS_KEY_ID,
      secret: process.env.AWS_SECRET_ACCESS_KEY || config.AWS_SECRET_ACCESS_KEY,
      bucket: 'audio-chat-files', // your bucket's name
      region: 'us-west-2' // your bucket's region
    });
    
    // Converts base64 data into a buffer
    var data = req.body.data.split(',').pop();
    var buffer = new Buffer(data, "base64");
    var headers = {'Content-Type': req.body.type};
    var path = '/' + req.body.name + '.wav';

    // The upload is held by the knox API
    client.putBuffer(buffer, path, headers, function(err, r) {
        var rsp = { upload: (r.statusCode == 200) };
        res.json(rsp);
    });
});
```

-------------------------------
## Running demo locally
To run this demo on your computer, first you'll need to [get a PeerJS API key](http://peerjs.com/peerserver) (it's free). If you want to uploade these conversations in your bucket at Amazon S3, you must get your bucket's information, such as, access key id, secret access key, bucket name and region.

Once you have this info, copy (or rename) `config.js.example` to `config.js`, open it and add your info there. You can also set these as config variables in your server.

Now just install dependencies and run the server:

```
$ npm install
$ node .\server.js
```

Open `http://localhost:6767` in your browser and you should be able to make calls.
