var express = require('express');
var router = express.Router();

try {
  var config = require('./config');
} catch (err) {
  var config = null;
}

var Call = require('./call');

var knox = require('knox');
var fs = require('fs');
var Buffer = require('buffer').Buffer;

// Knox client to access Amazon S3 bucket
var client = knox.createClient({
  key: process.env.AWS_ACCESS_KEY_ID || config.AWS_ACCESS_KEY_ID,
  secret: process.env.AWS_SECRET_ACCESS_KEY || config.AWS_SECRET_ACCESS_KEY,
  bucket: process.env.AMAZON_BUCKET_NAME || config.AMAZON_BUCKET_NAME,
  region: process.env.AMAZON_REGION || config.AMAZON_REGION
});

// Create a new Call instance, and redirect
router.get('/new', function(req, res) {
  var call = Call.create();
  res.redirect('/' + call.id);
});

router.post('/upload', function(req, res) {

  var data = req.body.data.split(',').pop();
  var buffer = new Buffer(data, "base64");
  var headers = {'Content-Type': req.body.type};
  var path = '/' + req.body.name + '.wav';

  client.putBuffer(buffer, path, headers, function(err, r) {
    if (r.statusCode != 200) console.log(r.statusCode + ': ' + r.statusMessage);

    var rsp = {
      upload: (r.statusCode == 200)
    };
    res.json(rsp);
  });

});

// Add PeerJS ID to Call instance when someone opens the page
router.post('/:id/addpeer/:peerid', function(req, res) {
  var call = Call.get(req.param('id'));
  if (!call) return res.status(404).send('Call not found');
  call.addPeer(req.param('peerid'));
  res.json(call.toJSON());
});

// Remove PeerJS ID when someone leaves the page
router.post('/:id/removepeer/:peerid', function(req, res) {
  var call = Call.get(req.param('id'));
  if (!call) return res.status(404).send('Call not found');
  call.removePeer(req.param('peerid'));
  res.json(call.toJSON());
});

// Return JSON representation of a Call
router.get('/:id.json', function(req, res) {
  var call = Call.get(req.param('id'));
  if (!call) return res.status(404).send('Call not found');
  res.json(call.toJSON());
});

// Render call page
router.get('/:id', function(req, res) {
  var call = Call.get(req.param('id'));
  if (!call) return res.redirect('/new');

  res.render('call', {
    apiKey: process.env.PEERJS_APIKEY || config.PEERJS_APIKEY,
    call: call.toJSON(),
    owner: Boolean(call.peers.length == 0)
  });
});

// Landing page
router.get('/', function(req, res) {
  res.render('index');
});

module.exports = router;