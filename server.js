var path = require('path');
var express = require('express');
var bodyParser = require("body-parser");

var routes = require('./routes');

var app = express();

app.use(bodyParser.json({limit: '100mb'}));
app.use(bodyParser.urlencoded({limit: '100mb', extended: true}));

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(express.static(path.join(__dirname, 'public')));
app.use('/', routes);

var port = process.env.PORT || 6767;
app.listen(port, function() {	
    console.log('Our app is running on http://localhost:' + port);
});